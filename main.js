/** Generate a random integer each frame and move the circle to it. **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 2 frames/sec.
    var game = new Core(320, 320);
    game.fps = 2;

    // Preload assets.
    game.preload('circle.png');
    
    // Specify what should happen when the game loads.
    game.onload = function () {
        var circle = new Sprite(48, 48);
        circle.image = game.assets['circle.png'];
        
        game.rootScene.backgroundColor = 'black';
        game.rootScene.addChild(circle);
    
        // Add an event listener to the circle Sprite to move it.
        circle.addEventListener(Event.ENTER_FRAME, function () {
            // Move the circle to a random place each frame.
            circle.x = randomInt(0, game.width - circle.width);
        });
    };
    
    // Start the game.
    game.start();
}

// === Helper functions === //
/** Generate a random integer between low and high (inclusive). */
function randomInt(low, high) {
    return low + Math.floor((high + 1 - low) * Math.random());
}
